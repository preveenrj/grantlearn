const routes = [
    {
        path: '/intercomback',
        method: 'GET',
        handler: async (request, reply) => {
            try {
                const response = request.yar.get("grant").response;
                console.log("response @@@",response);
                const accesstoken = response.access_token;
                console.log('received back here intercom  @@', accesstoken);
                return 'hello you are at safe hands';
                
            } catch (err) {
            }
        }
    },
{
    path: '/intercom-configure',
    method: 'POST',
    handler: async (request, reply) => {
        // console.log('configure payload ', request.payload);
        console.log('configure')
        try {
          const canvas = {
          content: {
            components: [
              {
                "type": "text",
                "id": "component-1",
                "text": "This is the first component",
                "align": "center",
                "style": "header"
              },
              {
                "type": "button",
                "id": "component-2",
                "label": "This is the second component",
                "action": {
                  "type": "submit"
                }
              }
            ]
          }
        };
            return { canvas };
            
        } catch (err) {
          console.log('err', err);
        }
    }
},
{
path: '/intercom-initialize',
method: 'POST',
handler: async (request, reply) => {
    console.log('ivide vann initialize');
    console.log('initialize payload ', request.payload);
    try {
      const components = [{
        "type": "text",
        "text": "*How likely are you to recommend this company to a friend or colleague?*",
        "id": "nps-question",
        "style": "header",
        "align": "center"
        },
        {
         "type":  "text",
         "text":  "Not Likely",
         "align": "left",
          "style": "muted"
        },
        {
          "type": "single-select",
          "id": "opinion-scale",
          "action": {
          "type": "sheet",
          "url": "https://c706b38f.ngrok.io/n/Preveen-NPS/tw-14f14c?email=usssdfer%40somecompany.com"
          },
          "options": [
            {
              "type": "option",
              "id": "score-0",
              "text": "0"
            },
            {
              "type": "option",
              "id": "score-1",
              "text": "1"
            },
            {
              "type": "option",
              "id": "score-2",
              "text": "2"
            },
            {
              "type": "option",
              "id": "score-3",
              "text": "3"
            },
            {
              "type": "option",
              "id": "score-4",
              "text": "4"
            },
            {
              "type": "option",
              "id": "score-5",
              "text": "5"
            },
            {
              "type": "option",
              "id": "score-6",
              "text": "6"
            },
            {
              "type": "option",
              "id": "score-7",
              "text": "7"
            },
            {
              "type": "option",
              "id": "score-8",
              "text": "8"
            },
            {
              "type": "option",
              "id": "score-9",
              "text": "9"
            },
            {
              "type": "option",
              "id": "score-10",
              "text": "10"
            }
          ]
        },
       {
         "type":  "text",
         "text":  "Most Likely",
         "align": "right",
         "style": "muted"
        },
 
  {
    "type": "spacer",
    "size": "xl"
  },
 {
    "type":  "text",
    "text":  "Powered by [SurveySparrow](https://surveysparrow.com)",
   "align": "center",
    "style": "paragraph"
  }
      ];
      const canvas = {
        content: {
          components
        }
      };
          return { canvas };
    } catch (err) {
    }
}
},
{
path: '/intercom-submit',
method: 'POST',
handler: async (request, reply) => {
    console.log('ivide vann submit');
    console.log('configure payload ', request.payload);
    try {
      const responseObj = {
        canvas:{
        content: {
          components: [
            {
              "type": "text",
              "id": "component-1",
              "text": "Submitted successfully",
              "align": "center",
              "style": "header"
            }
          ]
        }
      }
      };
          return responseObj;
    } catch (err) {
    }
}
}
  ];
  
  
module.exports = [
    ...routes
  ];