var Hapi = require('hapi')
var yar = require('yar')
var grant = require('grant-hapi')
var config = require('./config.json')
const axios = require('axios')
const  intercomRoutes = require('./intercomRoutes')
 
/*
NOTE:
to test the oauth in browser,
use: http://localhost:3000/connect/[provider] OR https://<NGROKLINK>/connect/[provider]
Private App Redirection URL: http://localhost:3000/connect/[provider]/callback OR https://<NGROKLINK>/connect/[provider]/callback
*/

const start = async () => {
    
    const server = new Hapi.Server({  
        host: 'localhost',
        port: 3000,
        load: { sampleInterval: 20000 }
      });
    
await server.register([
  // REQUIRED: any session store - see /examples/hapi-session-stores
  {plugin: yar, options: {cookieOptions: {password: 'this_is_a_some_bit_secure_password', isSecure: false}}},
  // mount grant
  {plugin: grant(), options: config }
]); 

    //Task route
    server.route([{
        path: '/',
        method: 'GET',
        handler: async (request, reply) => {
            return "Grant Learn"
        }
    },{
        path: '/hello',
        method: 'GET',
        handler: async (request, reply) => {
            try {
            const response = request.yar.get("grant").response;
            console.log("response @@@",response);
            const accesstoken = response.access_token;
            console.log('accesstoken ', accesstoken);
            const info = await axios({
                method: 'GET',
                url: 'http://api.surveysparrow.test/v1/contacts',
                headers: {
                    Authorization: `Bearer ${accesstoken}`,
                  }
              });
              console.log("contacts ",JSON.stringify(info.data.contacts));
            return info.data.contacts;
            }
            catch (err){
                console.log(err);
                return `error is ${err}`;
            }
        }
    },
    ...intercomRoutes
]);

    await server.start(function (err) {
        if (err){ 
            throw err;
        }
    })
    console.log("server started at "+ server.info.uri);
    
}
    start();
